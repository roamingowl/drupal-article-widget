'use strict';
import React from 'react';
import LanguageSelector from './languageSelector.jsx';
import Language from '../util/language.js'

export default class Article extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            language: Language.currentLangCode()
        }
    }

    onLanguageChange = langCode => this.setState({language: langCode});

    getTitle() {
        return this.state.language==='cs'?this.props.article.title:this.props.article.title_en
    }

    getBlockText(blk) {
        return this.state.language==='cs'?blk.content:blk.content_en;
    }

    render() {
        var self = this;
        return (
            <div>
                <h2>{this.getTitle()}</h2>
                {
                    this.props.article.blocks.map(function (blk) {
                        return (<div dangerouslySetInnerHTML={{__html: self.getBlockText(blk)}}></div>)
                    })
                }
                <br/>
                <LanguageSelector onChange={this.onLanguageChange} value={this.state.language}></LanguageSelector>
            </div>
        );
    }
}